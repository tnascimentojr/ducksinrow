package org.academiadecodigo.floppybirds.duckSniper;

import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Sniper {

    private Picture sniper;

    public Sniper() {
        sniper = new Picture(0, 0, ""); // X = background.getMaxWidth/2 Y = background.getMaxHeight
        sniper.draw();
    }

    public void shoot() {
        /*
        A ideia é dar o "tiro" e verificar se acertou. Construir if que invoca um método do Enemy (que alterará o estado booleano do inimigo).
        No inimigo a alteração do estado booleano, fará com o que o inimigo apareça ou desapareça.
        .drawn = show;
        .delete = hide;
        */
    }
}
